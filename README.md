# terraform-aws-rest-api-lambda-endpoint

Terraform module to create a Lambda-based REST API endpoint in AWS API Gateway.

## Caution!

You will need to use the depends_on meta argument to ensure that there will be no conflict during the creation of API Gateway *method*s, *integrations*, and *deployements*.  

Something like the following inside the Lambda endpoint module call. This will force Terraform to completely create the resources for the API itself before trying to create the resources for the Lambda endpoint.  

```hcl
depends_on = [
  module.rest-api
]
```

## Test Your API

If you're using the default demo files by placing them in your S3 bucket, you can test the API using the following command.

```bash
curl \
-X POST \
-H 'x-api-key: <api_key>' \
'<endpoint>?url=test.com'
```

The output should tell you that it has successfully sent a request to purge the cache for test.com.
